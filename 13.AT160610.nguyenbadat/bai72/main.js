const chay = () => {
  const a = window.document.question;
  let diem = 0;
  let isValid = true;
  const cau1 = window.document.querySelector(
    "input[type='radio'][name='cau1']:checked"
  )?.value;
  const cau2 = window.document.querySelector(
    "input[type='radio'][name='cau2']:checked"
  )?.value;
  const cau3 = window.document.querySelector(
    "input[type='radio'][name='cau3']:checked"
  )?.value;
  const arr = [];
  arr.push(cau1);
  arr.push(cau2);
  arr.push(cau3);
  
  for (let index = 0; index < arr.length; index++) {
    if (arr[index] === undefined) isValid = false;
  }
  if (!isValid) {
    alert("Vui lòng chọn đáp án");
    return;
  }
  for (let index = 0; index < arr.length; index++) {
    if (arr[index] === "true") diem++;
  }
  
  alert("Điểm của bạn là " + diem);
  //document.write("Hoàn thành bài thi với số điểm là " + diem);
};
