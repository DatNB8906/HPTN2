<div style="font-weight:bold;margin-bottom: 20px">Trang Draw Table</div>
<form id="formVe" action="" method="POST">
  <p style="margin-bottom:10px">Form ve bang</p>
  <div style="margin-bottom:10px">
    <label for="soDong">So dong: </label>
    <input type="text" name="soDong">
  </div>
  <div style="margin-bottom:10px">
    <label for="soCot">So cot: </label>
    <input type="text" name="soCot">
  </div>
  <div style="margin-bottom:10px">
    <button style="margin-right:5px" type="reset">Nhap lai</button>
    <button type="submit">Ve</button>
  </div>
</form>

<?php 
  if( !empty($_POST['soDong']) && !empty($_POST['soCot']) ) {
    echo "<table style='width:100%';>";
    for ($i = 0; $i < (int)$_POST['soDong']; $i++) {
      echo "<tr>";
      for ($j = 0; $j < (int)$_POST['soCot']; $j++) {
        echo "<td>",$j + 1,"</td>";
      }
      echo "</tr>";
    }

    echo "</table>";
  }
?>