<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./style.css">
    <script src="./main.js" defer></script>
</head>

<body>
    <?php include_once('../header.php') ?>
    <header>
        <nav>
            <form id="form" action="" method="get">
                <input id="input" hidden type="text" name="page">
                <button data-page="caculate" type="button">Caculate</button>
                <button data-page="register" type="button">Register</button>
            </form>
        </nav>
    </header>
    <main>
      <?php include_once('../left.php') ?>
        <div class="right" style="margin-top: 50px;">
            <?php
        if(!empty($_GET['page'])) {
          switch($_GET['page']) {
            case "caculate":
              include_once("./pages/caculate.php");
              break;
            case "register":
              include_once("./pages/register.php");
              break;
          }
        } else {
          echo "No page";
        }
      ?>
        </div>
    </main>
</body>

</html>