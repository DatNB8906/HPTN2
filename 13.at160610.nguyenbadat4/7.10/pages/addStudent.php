<form action="" id="form1" method="post">
  <div style="font-weight:bold">Them sinh vien moi</div>
  <div class="field">
    <div>
      <label for="">Ten:</label>
    </div>
    <div>
      <input type="text" name="ten">
    </div>
  </div>
  <div class="field">
    <div>
      <label for="">Dia chi:</label>
    </div>
    <div>
      <input type="text" name="diaChi">
    </div>
  </div>
  <div class="field">
    <div>
      <label for="">Tuoi:</label>
    </div>
    <div>
      <input type="text" name="tuoi">
    </div>
  </div>
  <div>
    <button type="reset">Nhap lai</button>
    <button type="submit">Ghi</button>
  </div>
</form>

<?php 
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(empty($_POST["ten"]) || empty($_POST["diaChi"]) || empty($_POST["tuoi"])) {
      echo "<p style='color: red'>Cac truong ko duoc de thieu</p>";
    } else {
      $fp = fopen('student.txt', 'a');
      fputs($fp, "\n".$_POST["ten"]);
      fputs($fp, "\n".$_POST["diaChi"]);
      fputs($fp, "\n".$_POST["tuoi"]);
      fclose($fp);
      echo "Ghi thanh cong.";
    }
  }
?>

<style>
  .field{
    display:flex;
    width:250px;
    justify-content: space-between;
    margin-bottom: 10px
  }
</style>